$(".owl-carousel").owlCarousel({
    items: 5,
    loop: true,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    margin: 20,
    autoWidth: true,
    responsiveClass: true,
    responsive:{
        // Razlog problema: Nedostaje početna responzivnost ili neka druga minimalna rezolucija
        0: { // u prijevodu: od 0 do donjeg (768)
            items: 1,
            margin: 0, // nepotrebno za prikaz 1 elementa, jer nema ga od cega odvojiti ako je sam na prikazu
            autoWidth: false // također, jer onda slider pobjegne
        },
        768: { // u tvom primjeru nije bilo 0 pa se responzivnost odnosila na rezolucije od 768 do 1400, tako da mobilne verzije nisu bile obuhvaćene
            items: 1,
        },
        1400: {
            items: 1,
            autoWidth:true
        }
    }
});

var sliders=document.getElementsByClassName("slider");
for(var i=0;i<sliders.length;i++){
    var element=sliders[i];
    element.oninput=function(){ 
        var min=this.min,max=this.max;

        var raspon = max - min;
        var value=this.value*100/this.max; 
        if(this.max==2020){
            value=(this.value-min)*100/(raspon); 
        }
        this.style.background="linear-gradient(to right, #00def8 0%, #18b3fa "+value+"%, #f4fafa "+value+"%,#f4fafa 100%)";
    }
}

$("#nav__menu div").click(function(){
    $("#nav__bar").toggle('slow');  
});

function checkWidth() {
    var windowWidth=window.innerWidth;
    if (windowWidth > 990) {
        $("#nav__bar").show();
    }
}

window.addEventListener("resize", checkWidth);

$('#vehicle_options').on('click', '.vehicle__option--choose', function() {
    $(this).toggleClass('vehicle__option--choose__exception');
});

var modal = new tingle.modal({
    footer: true,
    stickyFooter: false,
    closeMethods: ['overlay', 'button', 'escape'],
    closeLabel: "Close"
});

// set content
modal.setContent('<h1>here\'s some content</h1>');

// add a button
modal.addFooterBtn('close', 'tingle-btn tingle-btn--primary', function() {
    // here goes some logic
    modal.close();
});

document.getElementById("nav__bar--button").addEventListener('click',function(){
    console.log('open');
    modal.open();
});



function changeValuePrice(val, type){
    if (type == 'price') {
        $(".priceLabel span").html(val); 
    }else if (type == 'year') {
        $(".yearLabel span").html(val); 
    }else if (type == 'mileage') {
        $(".mileageLabel span").html(val); 
    }
}

